const body = document.getElementById('null');
const max = 0;

const grid = [
    [3, 0, 8, 4],
    [2, 4, 5, 7],
    [9, 2, 6, 3],
    [0, 3, 1, 0],
] //35
// const grid = [
//     [0, 0, 0, 0],
//     [0, 0, 0, 0],
//     [0, 0, 0, 0],
//     [0, 0, 0, 0],
// ] 
const maxIncrease = (grid) => {
    let increase = 0;
    let arr = [];

    let verticalHorizon = [];
    let horizon = [];

    for (let i = 0; i < grid.length; i++) {
        const element = grid[i];
        arr.push([]);
        horizon.push(Math.max(...element)); 
    }

    for (let i = 0; i < grid.length; i++) {
        const element = grid[i];
        for (let j = 0; j < element.length; j++) {
            arr[j].push(grid[i][j]);
        }
    }

    for (let i = 0; i < arr.length; i++) {
        const element = arr[i];
        verticalHorizon.push(Math.max(...element));
    }

    for (let i = 0; i < grid.length; i++) {
        const element = grid[i];
        for (let j = 0; j < element.length; j++) {
            const el = element[j];
            let hor = horizon[i];
            let ver = verticalHorizon[j];
            increase = increase + Math.min(hor, ver) - el; 
        }
    }
    console.log(increase);
    return increase;
} 

//maxIncrease(grid);

body.append(maxIncrease(grid));